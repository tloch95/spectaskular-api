FROM python:3.7.7-slim
WORKDIR /usr/src/app
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY ./project ./project
COPY run.py .
COPY ./migrations ./migrations
COPY ./docker-entrypoint.sh /
EXPOSE 5000
RUN useradd -m myuser
USER myuser
#RUN chmod u+x /docker-entrypoint.sh
#RUN chown $USER /postgres_data
ENTRYPOINT ["/docker-entrypoint.sh"]