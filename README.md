# spectaskular

A light weight to-do list/task management app to keep track of your complete and incomplete tasks and projects.

At the moment, the project uses a Postgres database for persistence. The project also uses Flask for the API endpoints, Docker to containerize the project, Heroku to deploy, and will use React.js in the future for the front end.

## Install

#### Virtualenv

All work should be done inside of a virtual environment. I like to use `python3 -m venv env` to make sure my virtual environment is created using python3.

Once your virtual environment is created, activate it using `source env/bin/activate`. Now you can install the project requirements using `pip install -r requirements.txt`.

### Docker
In order to get the project running, docker-compose is used to build/run the Flask backend and the Postgres database. Therefore, Docker, and docker-compose are both required. To bring the container up in detached mode, use the command

```
docker-compose up --build -d
```
in the root directory of the project.

To bring the container down, use the command
```
docker-compose down
```

### Local

In order to run the project locally, use the ```docker-compose up --build -d``` command to bring up the app and db.

Once the containers are built and running, use the ```docker stop web``` command to stop the app, to free up the port for running locally.

*Note: You will need to change your db url to use localhost for this next part*

Now, run this command to run the application locally:
```
python3 -m flask run
```

You should now see the app run locally. If you would like to enable debug mode, use this commmand:
```
export FLASK_ENV=development
```

## Migrations

To create a migration is simple. First, make your changes to `models.py`, reference [this website](https://flask-sqlalchemy.palletsprojects.com/en/2.x/models/) if you need help doing so. Once you are satisfied with your model modifications, run this command locally (note - you will need to change your database URL to use localhost for the host for the time being):
```
flask db migrate -m "Optional message"
```
*Note: If flask command is not found, you can also use ```python3 -m flask db migrate -m "Optional message"``` in its place.*

This will create a new migration file. Review the newly created migration file to be sure it is as you expect. If you are satisfied with the changes, you are done and can bring down the Docker container(s) and bring them back up to apply the changes to the database (**make sure to change the db name back from localhost**).

NOTE: Currently, if a column is set to nullable and there is existing data in the database, the easiest way to apply the migration is to just wipe out the database. This is obviously highly undesired, and will need to find a solution for this.