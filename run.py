"""
Our Flask entrypoint
"""
import os

from project.api import app

if __name__ == '__main__':
    # Need to use host for Docker container to be
    # accessible from outside container.
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
