"""
The modules that holds all of our models
"""
from datetime import datetime
import uuid

from sqlalchemy.dialects.postgresql import UUID

from project.api import db

class User(db.Model):
    """User model

    Args:
        db: Our db instance

    Columns:
        id: The id of the user
        username: The username of the user
        email: The email of the user
        password: The encrypted password of the user
        created: The date and time that the user was created
        updated: The date and time that the user was most recently updated
        projects: A relation to all projects owned by this user
    """
    id = db.Column(UUID(as_uuid=True),
                   primary_key=True,
                   default=uuid.uuid4,
                   unique=True,
                   nullable=False)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(200), nullable=False)
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    projects = db.relationship('Project', backref='owner', lazy=True)

    def __repr__(self):
        return f"User({self.id}, {self.username}, {self.email})"

class Project(db.Model):
    """Project model

    Args:
        db: Our db instance

    Columns:
        id: The id of the project
        name: The name of this project
        description: The description of this project
        created: The date and time that the project was created
        updated: The date and time that the project was most recently updated
        user_id: The id of the user that owns this project
        tasks: A relation to all tasks owned by this project
        parent_project_id: The id of the parent project
        children_projects: A relation to all children projects to this project
    """
    id = db.Column(UUID(as_uuid=True),
                   primary_key=True,
                   default=uuid.uuid4,
                   unique=True,
                   nullable=False)
    name = db.Column(db.String(80), nullable=False)
    description = db.Column(db.String(500), nullable=True)
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey('user.id'), nullable=False)
    tasks = db.relationship('Task', backref='project', lazy=True)
    parent_project_id = db.Column(UUID(as_uuid=True), db.ForeignKey('project.id'), nullable=True)
    children_projects = db.relationship("Project")

    def __repr__(self):
        return f"Project({self.id}, {self.name}, {self.created})"

class Task(db.Model):
    """Task model

    Args:
        db: Our db instance

    Columns:
        id: The id of this task
        title: The title of this task
        description: The description of this task
        completed: T/F - whether or not this task is complete
        created: The date and time that the task was created
        updated: The date and time that the task was most recently updated
        user_id: The id of the user that owns this task
        project_id: The id of the project that owns this task
        comments: A relation to all comments owned by this task
        parent_task_id: The id of the parent task
        children_tasks: A relation to all children tasks to this task
    """
    id = db.Column(UUID(as_uuid=True),
                   primary_key=True,
                   default=uuid.uuid4,
                   unique=True,
                   nullable=False)
    title = db.Column(db.String(150), nullable=False)
    description = db.Column(db.String(2500), nullable=True)
    completed = db.Column(db.Boolean, nullable=False, default=False)
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey('user.id'), nullable=False)
    project_id = db.Column(
        UUID(as_uuid=True),
        db.ForeignKey('project.id'),
        nullable=False
    )
    comments = db.relationship('Comment', backref='task', lazy=True)
    parent_task_id = db.Column(UUID(as_uuid=True),
                               db.ForeignKey('task.id'),
                               unique=True,
                               nullable=True)
    children_tasks = db.relationship("Task")

    def __repr__(self):
        return f"Task({self.id}, '{self.title}', {self.created})"

class Comment(db.Model):
    """Comment model

    Args:
        db: Our db instance

    Columns:
        id: The id of this comment
        text: The text value of this comment
        created: The date and time that the comment was created
        updated: The date and time that the comment was most recently updated
        user_id: The id of the user that owns this comment
        task_id: The id of the task that owns this comment
        parent_comment_id: The id of the parent comment
        children_comments: A relation to all children comments to this comment
    """
    id = db.Column(UUID(as_uuid=True),
                   primary_key=True,
                   default=uuid.uuid4,
                   unique=True,
                   nullable=False)
    text = db.Column(db.String(1000), nullable=False)
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    user_id = db.Column(UUID(as_uuid=True), db.ForeignKey('user.id'), nullable=False)
    task_id = db.Column(UUID(as_uuid=True), db.ForeignKey('task.id'), nullable=False)
    parent_comment_id = db.Column(UUID(as_uuid=True), db.ForeignKey('comment.id'), nullable=True)
    children_comments = db.relationship("Comment")

    def __repr__(self):
        return f"Comment({self.id}, {self.created})"

class BlacklistedToken(db.Model):
    """Model for tokens that have been blacklisted

    Args:
        db: Our db instance

    Columns:
        id: The id of this blacklisted token
        token: The token value that is blacklisted
    """
    id = db.Column(UUID(as_uuid=True),
                   primary_key=True,
                   default=uuid.uuid4,
                   unique=True,
                   nullable=False)
    token = db.Column(db.String(500), nullable=False)
