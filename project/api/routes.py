"""
This module holds all routes for the Spectaskular API.
May be broken up into categories using Flask blueprints in the future.
https://flask.palletsprojects.com/en/1.1.x/tutorial/views/

Categories:
auth, user, project, task, comment
"""
from datetime import datetime
import flask
import json
import os
import uuid

from flask import request, Response
from werkzeug.security import generate_password_hash, check_password_hash
from flask_jwt_extended import (
    create_access_token, create_refresh_token,
    get_jwt_identity, get_jwt,
    jwt_required, set_refresh_cookies
)

from project.api import ACCESS_EXPIRES, app, db, jwt
from project.api.models import BlacklistedToken, Comment, Project, Task, User
from project.utils import (
    get_uuid_hex
)

@app.route('/', methods=['GET'])
def health():
    """Simple health check route
    Maybe this can be expanded to run tests on Spectaskular?

    Returns:
        str: A healthy message
    """
    return "Welcome to Spectaskular! Looking healthy!"

##### AUTH ROUTES #####
@jwt.token_in_blocklist_loader
def check_if_token_revoked(jwt_header, jwt_payload):
    """Check if a token is blacklisted.

    Args:
        decrypted_token (unknown): Decrypted token passed in by flask-jwt-extended

    Returns:
        boolean: True if the token is blacklisted, else False
    """
    jti = jwt_payload["jti"]
    token = db.session.query(BlacklistedToken.id).filter_by(token=jti).scalar()
    return token is not None

@app.route('/login', methods=['POST'])
def login():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the user data from the request
    email = request.form.get('email')
    password = request.form.get('password')
    # remember = True if request.form.get('remember') else False

    # Check if User exists in db, and the hashed password
    # matches the hashed password in the database
    user = User.query.filter_by(email=email).first()
    if not user or not check_password_hash(user.password, password):
        return Response(status=422)

    # Send our success response
    access_token = create_access_token(identity=get_uuid_hex(user.id), fresh=True)
    refresh_token = create_refresh_token(identity=get_uuid_hex(user.id))
    response_dict = {
        'success': True,
        'username': user.username,
        'user_id': get_uuid_hex(user.id),
        'access_token': access_token,
        'refresh_token': refresh_token,
        'exp': str(datetime.utcnow() + ACCESS_EXPIRES)
    }

    response = app.response_class(
        response=json.dumps(response_dict),
        mimetype='application/json'
    )

    # Useful stuff if we cannot figure out how to proxy in production
    # response.headers['Access-Control-Allow-Origin'] = 'localhost'
    # response.headers['Access-Control-Allow-Credentials'] = 'true'
    
    # resp = flask.Response()
    # resp.headers['Access-Control-Allow-Origin'] = 'localhost'

    set_refresh_cookies(response, refresh_token)
    return response

@app.route('/demo', methods=['POST'])
def demo():
    """[summary]

    Returns:
        [type]: [description]
    """
    # If demo user doesn't exist yet, create it.
    demo_user = User.query.filter_by(email=os.environ['DEMO_EMAIL']).first()
    if not demo_user:
        demo_user = User(username=os.environ['DEMO_USER'],
                         password=generate_password_hash(os.environ['DEMO_PASS'], method='sha256'),
                         email=os.environ['DEMO_EMAIL'])
        db.session.add(demo_user)
        db.session.commit()

    # Send our success response
    access_token = create_access_token(identity=demo_user.id, fresh=True)
    response_dict = {
        'success': True,
        'username': demo_user.username,
        'user_id': get_uuid_hex(demo_user.id),
        'access_token': access_token,
        'exp': str(datetime.utcnow() + ACCESS_EXPIRES)
    }

    response = app.response_class(
        response=json.dumps(response_dict),
        mimetype='application/json'
    )
    return response

@app.route('/refresh', methods=['POST'])
@jwt_required(refresh=True, locations=['cookies'])
def refresh():
    """[summary]

    Returns:
        [type]: [description]
    """
    current_user = get_jwt_identity()
    response_dict = {
        'success': True,
        'access_token': create_access_token(identity=current_user, fresh=False),
        'exp': str(datetime.utcnow() + ACCESS_EXPIRES)
    }
    response = app.response_class(
        response=json.dumps(response_dict),
        mimetype='application/json'
    )
    return response

@app.route('/register', methods=['POST'])
def register():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the user data from the request
    username = request.form.get('username')
    email = request.form.get('email')
    password = request.form.get('password')

    # Make sure this email hasn't been used
    user = User.query.filter_by(email=email).first()
    if user:
        return Response(status=409, mimetype='application/json')

    # Make sure this usernameisn't taken
    user = User.query.filter_by(username=username).first()
    if user:
        # Probably want to change this to return something
        # consumable by the front end
        return "Username taken"

    # Add the new user to the db
    new_user = User(
        email=email,
        username=username,
        password=generate_password_hash(password, method='sha256')
    )
    db.session.add(new_user)
    db.session.commit()

    # Send our success response
    response_dict = {'success': True}
    response = app.response_class(
        response=json.dumps(response_dict),
        mimetype='application/json'
    )
    return response

@app.route('/logout/access', methods=['POST'])
@jwt_required()
def access_logout():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Add token to our blacklisted tokens
    jti = get_jwt()['jti']
    token = BlacklistedToken(token=jti)
    db.session.add(token)
    db.session.commit()

    # Send our success response
    response_dict = {'success': True}
    response = app.response_class(
        response=json.dumps(response_dict),
        mimetype='application/json'
    )
    return response

@app.route('/logout/refresh', methods=['POST'])
@jwt_required(refresh=True, locations=['cookies'])
def refresh_logout():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Add token to our blacklisted tokens
    jti = get_jwt()['jti']
    token = BlacklistedToken(token=jti)
    db.session.add(token)
    db.session.commit()

    # Send our success response
    response_dict = {'success': True}
    response = app.response_class(
        response=json.dumps(response_dict),
        mimetype='application/json'
    )
    return response

##### USER ROUTES #####
# @app.route('/user/create', methods=['POST'])
# @jwt_required()
# def create_user():
#     """[summary]

#     Returns:
#         [type]: [description]
#     """
#     # Get the user info
#     username = request.args.get('user')
#     email = request.args.get('email')
#     password = request.args.get('pass')

#     # Create the new user
#     new_user = User(username=username, password=password, email=email)
#     db.session.add(new_user)
#     db.session.commit()

#     # Some useful info to return
#     user_dict = {}
#     user_dict['id'] = get_uuid_hex(new_user.id)
#     user_dict['username'] = new_user.username
#     user_dict['email'] = new_user.email
#     user_dict['created'] = str(new_user.created)
#     user_dict['updated'] = str(new_user.updated)
#     return json.dumps(user_dict), 200

# @app.route('/user/read', methods=['GET'])
# @jwt_required()
# def read_user():
#     """[summary]

#     Returns:
#         [type]: [description]
#     """
#     # Get the user id
#     user_id = request.args.get('id')

#     # Do not proceed if the current user is not the one being read.
#     if uuid.UUID(get_jwt_identity()) != uuid.UUID(user_id):
#         return Response(status=401, mimetype='application/json')

#     # Get the user
#     user = User.query.get(user_id)
#     if user is None:
#         return "That user id does not exist.", 400

#     # Some useful info to return
#     user_dict = {}
#     user_dict['id'] = get_uuid_hex(user.id)
#     user_dict['username'] = user.username
#     user_dict['email'] = user.email
#     user_dict['created'] = str(user.created)
#     user_dict['updated'] = str(user.updated)
#     return json.dumps(user_dict), 200

# @app.route('/user/update', methods=['POST'])
# @jwt_required()
# def update_user():
#     """[summary]

#     Returns:
#         [type]: [description]
#     """
#     # Get the user id
#     user_id = request.args.get('id')

#     # Do not proceed if this is not this user's data.
#     if uuid.UUID(get_jwt_identity()) != uuid.UUID(user_id):
#         return Response(status=401, mimetype='application/json')

#     # Get the user and update it
#     user = User.query.get(user_id)
#     if user is None:
#         return "That user id does not exist.", 400

#     user.username = request.args.get('user')
#     user.email = request.args.get('email')
#     user.password = request.args.get('pass')
#     user.updated = datetime.utcnow()
#     db.session.commit()

#     # Some useful info to return
#     user_dict = {}
#     user_dict['id'] = get_uuid_hex(user.id)
#     user_dict['username'] = user.username
#     user_dict['email'] = user.email
#     user_dict['created'] = str(user.created)
#     user_dict['updated'] = str(user.updated)
#     return json.dumps(user_dict), 200

# @app.route('/user/delete', methods=['DELETE'])
# @jwt_required()
# def delete_user():
#     """[summary]

#     Returns:
#         [type]: [description]
#     """
#     # Get the user id
#     user_id = request.args.get('id')

#     # Do not proceed if this is not this user's data.
#     if uuid.UUID(get_jwt_identity()) != uuid.UUID(user_id):
#         return Response(status=401, mimetype='application/json')

#     # Get the user and delete it
#     user = User.query.get(user_id)
#     if user is None:
#         return "That user id does not exist.", 400

#     db.session.delete(user)
#     db.session.commit()

#     return Response(status=200, mimetype='application/json')

##### PROJECT ROUTES #####
@app.route('/project/create', methods=['POST'])
@jwt_required()
def create_project():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the project info
    name = request.args.get('name')
    desc = request.args.get('desc')
    parent = request.args.get('parent', None)

    # Create the new project
    new_proj = Project(
        name=name,
        description=desc,
        user_id=get_jwt_identity(),
        parent_project_id=(None if parent is None else parent)
    )
    db.session.add(new_proj)
    db.session.commit()

    # Some useful info to return
    proj_dict = {}
    proj_dict['id'] = get_uuid_hex(new_proj.id)
    proj_dict['name'] = new_proj.name
    proj_dict['description'] = new_proj.description
    proj_dict['created'] = str(new_proj.created)
    proj_dict['updated'] = str(new_proj.updated)
    proj_dict['owner'] = get_uuid_hex(new_proj.user_id)
    proj_dict['parent_project_id'] = get_uuid_hex(new_proj.parent_project_id)
    return json.dumps(proj_dict), 200

@app.route('/project/read', methods=['GET'])
@jwt_required()
def read_project():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the project
    proj_id = request.args.get('id')
    proj = Project.query.get(proj_id)

    if proj is None:
        return "That project id does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != proj.user_id:
        return Response(status=401, mimetype='application/json')

    # Some useful info to return
    proj_dict = {}
    proj_dict['id'] = get_uuid_hex(proj.id)
    proj_dict['name'] = proj.name
    proj_dict['description'] = proj.description
    proj_dict['created'] = str(proj.created)
    proj_dict['updated'] = str(proj.updated)
    proj_dict['owner'] = get_uuid_hex(proj.user_id)
    proj_dict['parent'] = get_uuid_hex(proj.parent_project_id)
    return json.dumps(proj_dict), 200

@app.route('/project/update', methods=['POST'])
@jwt_required()
def update_project():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the project
    proj_id = request.args.get('id')
    proj = Project.query.get(proj_id)

    if proj is None:
        return "That project id does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != proj.user_id:
        return Response(status=401, mimetype='application/json')

    proj.name = request.args.get('name')
    proj.description = request.args.get('desc')
    if request.args.get('parent') is not None:
        # Update to the newly specified parent project if it was specified
        proj.parent_project_id = request.args.get('parent')
    proj.updated = datetime.utcnow()
    db.session.commit()

    # Some useful info to return
    proj_dict = {}
    proj_dict['id'] = get_uuid_hex(proj.id)
    proj_dict['name'] = proj.name
    proj_dict['description'] = proj.description
    proj_dict['created'] = str(proj.created)
    proj_dict['updated'] = str(proj.updated)
    proj_dict['owner'] = get_uuid_hex(proj.user_id)
    proj_dict['parent'] = get_uuid_hex(proj.parent_project_id)
    return json.dumps(proj_dict), 200

@app.route('/project/delete', methods=['DELETE'])
@jwt_required()
def delete_project():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the project
    proj_id = request.args.get('id')
    proj = Project.query.get(proj_id)

    if proj is None:
        return "That project id does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != proj.user_id:
        return Response(status=401, mimetype='application/json')

    # Delete the associated tasks
    tasks = Task.query.filter_by(project_id=proj_id).all()
    for task in tasks:
        db.session.delete(task)

    db.session.delete(proj)
    db.session.commit()

    return Response(status=200, mimetype='application/json')

##### PROJECTS ROUTES #####
@app.route('/projects/read', methods=['GET'])
@jwt_required()
def read_projects():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the owner id
    user_id = request.args.get('owner_id')

    if user_id is None:
        return 'An owner id was not specified.', 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != uuid.UUID(user_id):
        return Response(status=401, mimetype='application/json')

    # Get the projects for that user
    projects = Project.query.filter_by(user_id=user_id).order_by(Project.created).all()
    projects_list = []
    for proj in projects:
        proj_dict = {}
        proj_dict['id'] = get_uuid_hex(proj.id)
        proj_dict['name'] = proj.name
        proj_dict['description'] = proj.description
        proj_dict['created'] = str(proj.created)
        proj_dict['updated'] = str(proj.updated)
        proj_dict['parent'] = get_uuid_hex(proj.parent_project_id)
        projects_list.append(proj_dict)

    return json.dumps(projects_list), 200

##### TASK ROUTES #####
@app.route('/task/create', methods=['POST'])
@jwt_required()
def create_task():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the task info
    title = request.args.get('title')
    desc = request.args.get('desc')
    completed = request.args.get('completed') == 'true'
    project = request.args.get('project')
    parent = request.args.get('parent', None)

    # Get the project for this task
    proj = Project.query.get(project)

    if proj is None:
        return "That project id does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != proj.user_id:
        return Response(status=401, mimetype='application/json')

    # Create the new task
    new_task = Task(
        title=title,
        description=desc,
        completed=completed,
        project_id=project,
        parent_task_id=(None if parent is None else parent),
        user_id=get_jwt_identity()
    )
    db.session.add(new_task)
    db.session.commit()

    # Some useful info to return
    task_dict = {}
    task_dict['id'] = get_uuid_hex(new_task.id)
    task_dict['title'] = new_task.title
    task_dict['description'] = new_task.description
    task_dict['completed'] = new_task.completed
    task_dict['created'] = str(new_task.created)
    task_dict['updated'] = str(new_task.updated)
    task_dict['project_id'] = get_uuid_hex(new_task.project_id)
    task_dict['parent_task_id'] = get_uuid_hex(new_task.parent_task_id)
    task_dict['owner'] = get_uuid_hex(new_task.user_id)
    return json.dumps(task_dict), 200

@app.route('/task/read', methods=['GET'])
@jwt_required()
def read_task():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the task
    task_id = request.args.get('id')
    task = Task.query.get(task_id)

    if task is None:
        return "That task id does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != task.user_id:
        return Response(status=401, mimetype='application/json')

    # Some useful info to return
    task_dict = {}
    task_dict['id'] = get_uuid_hex(task.id)
    task_dict['title'] = task.title
    task_dict['description'] = task.description
    task_dict['completed'] = task.completed
    task_dict['created'] = str(task.created)
    task_dict['updated'] = str(task.updated)
    task_dict['project'] = get_uuid_hex(task.project_id)
    task_dict['parent'] = get_uuid_hex(task.parent_task_id)
    task_dict['owner'] = task.user_id
    return json.dumps(task_dict), 200

@app.route('/task/update', methods=['POST'])
@jwt_required()
def update_task():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the task
    task_id = request.args.get('id')
    task = Task.query.get(task_id)

    if task is None:
        return "That task id does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != task.user_id:
        return Response(status=401, mimetype='application/json')

    task.title = request.args.get('title')
    task.description = request.args.get('desc')
    task.completed = request.args.get('completed') == 'true'
    if request.args.get('project') is not None:
        # Update to the newly specified project if it was specified
        task.project_id = request.args.get('project')
    if request.args.get('parent') is not None:
        # Update to the newly specified parent task if it was specified
        task.parent_task_id = request.args.get('parent')
    task.updated = datetime.utcnow()
    db.session.commit()

    # Some useful info to return
    task_dict = {}
    task_dict['id'] = get_uuid_hex(task.id)
    task_dict['name'] = task.title
    task_dict['description'] = task.description
    task_dict['completed'] = task.completed
    task_dict['created'] = str(task.created)
    task_dict['updated'] = str(task.updated)
    task_dict['project'] = get_uuid_hex(task.project_id)
    task_dict['parent'] = get_uuid_hex(task.parent_task_id)
    task_dict['owner'] = get_uuid_hex(task.user_id)
    return json.dumps(task_dict), 200

@app.route('/task/delete', methods=['DELETE'])
@jwt_required()
def delete_task():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the task
    task_id = request.args.get('id')
    task = Task.query.get(task_id)

    if task is None:
        return "That task id does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != task.user_id:
        return Response(status=401, mimetype='application/json')

    db.session.delete(task)
    db.session.commit()

    return Response(status=200, mimetype='application/json')

@app.route('/task/complete', methods=['POST'])
@jwt_required()
def complete_task():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the task
    task_id = request.args.get('id')
    task = Task.query.get(task_id)

    if task is None:
        return "That task id does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != task.user_id:
        return Response(status=401, mimetype='application/json')

    task.completed = request.args.get('completed') == 'true'
    task.updated = datetime.utcnow()
    db.session.commit()

    # Some useful info to return
    task_dict = {}
    task_dict['id'] = get_uuid_hex(task.id)
    task_dict['name'] = task.title
    task_dict['description'] = task.description
    task_dict['completed'] = task.completed
    task_dict['created'] = str(task.created)
    task_dict['updated'] = str(task.updated)
    task_dict['project'] = get_uuid_hex(task.project_id)
    task_dict['parent'] = get_uuid_hex(task.parent_task_id)
    task_dict['owner'] = get_uuid_hex(task.user_id)
    response = app.response_class(
        response=json.dumps(task_dict),
        mimetype='application/json'
    )
    return response

##### TASKS ROUTES #####
@app.route('/tasks/read', methods=['GET'])
@jwt_required()
def read_tasks():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the project id
    project_id = request.args.get('project_id')

    if project_id is None:
        return 'A project id was not specified.', 400

    # Get the project for this task
    proj = Project.query.get(project_id)
    if proj is None:
        return "The project for this task does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != proj.user_id:
        return Response(status=401, mimetype='application/json')

    # Get the projects for that user
    tasks = Task.query.filter_by(project_id=project_id).order_by(Task.created).all()
    tasks_list = []
    for task in tasks:
        task_dict = {}
        task_dict['id'] = get_uuid_hex(task.id)
        task_dict['title'] = task.title
        task_dict['description'] = task.description
        task_dict['completed'] = task.completed
        task_dict['created'] = str(task.created)
        task_dict['updated'] = str(task.updated)
        task_dict['parent'] = get_uuid_hex(task.parent_task_id)
        task_dict['owner'] = get_uuid_hex(proj.user_id)
        tasks_list.append(task_dict)

    return json.dumps(tasks_list), 200

##### COMMENT ROUTES #####
@app.route('/comment/create', methods=['POST'])
@jwt_required()
def create_comment():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the comment info
    text = request.args.get('text')
    task_id = request.args.get('task')
    parent = request.args.get('parent', None)

    if task_id is None:
        return 'A task id was not specified.', 400

    # Get the task for this comment
    task = Task.query.get(task_id)
    if task is None:
        return "The task for this comment does not exist.", 400

    # Get the project for this task
    proj = Project.query.get(task.project_id)
    if proj is None:
        return "The project associated with the task for this comment does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != proj.user_id:
        return Response(status=401, mimetype='application/json')

    # Create the new comment
    new_comment = Comment(
        text=text,
        task_id=task,
        parent_comment_id=(None if parent is None else parent),
        user_id=get_jwt_identity()
    )
    db.session.add(new_comment)
    db.session.commit()

    # Some useful info to return
    comment_dict = {}
    comment_dict['id'] = get_uuid_hex(new_comment.id)
    comment_dict['text'] = new_comment.text
    comment_dict['created'] = str(new_comment.created)
    comment_dict['updated'] = str(new_comment.updated)
    comment_dict['task_id'] = get_uuid_hex(new_comment.task_id)
    comment_dict['parent_comment_id'] = get_uuid_hex(new_comment.parent_comment_id)
    comment_dict['owner'] = new_comment.user_id
    return json.dumps(comment_dict), 200

@app.route('/comment/read', methods=['GET'])
@jwt_required()
def read_comment():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the comment id
    comment_id = request.args.get('id')

    # Get the comment
    comment = Comment.query.get(comment_id)
    if comment is None:
        return "That comment id does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != comment.user_id:
        return Response(status=401, mimetype='application/json')

    # Some useful info to return
    comment_dict = {}
    comment_dict['id'] = get_uuid_hex(comment.id)
    comment_dict['text'] = comment.text
    comment_dict['created'] = str(comment.created)
    comment_dict['updated'] = str(comment.updated)
    comment_dict['task_id'] = get_uuid_hex(comment.task_id)
    comment_dict['parent_comment_id'] = get_uuid_hex(comment.parent_comment_id)
    comment_dict['owner'] = comment.user_id
    return json.dumps(comment_dict), 200

@app.route('/comment/update', methods=['POST'])
@jwt_required()
def update_comment():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the comment
    comment_id = request.args.get('id')
    comment = Comment.query.get(comment_id)
    if comment is None:
        return "That comment id does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != comment.user_id:
        return Response(status=401, mimetype='application/json')

    comment.text = request.args.get('text')
    comment.task_id = comment.task_id
    if request.args.get('task') is not None:
        # Update to the newly specified task if it was specified
        comment.task_id = request.args.get('task')
    if request.args.get('parent') is not None:
        # Update to the newly specified parent comment if it was specified
        comment.parent_comment_id = request.args.get('parent')
    comment.updated = datetime.utcnow()
    db.session.commit()

    # Some useful info to return
    comment_dict = {}
    comment_dict['id'] = get_uuid_hex(comment.id)
    comment_dict['text'] = comment.text
    comment_dict['created'] = str(comment.created)
    comment_dict['updated'] = str(comment.updated)
    comment_dict['task_id'] = get_uuid_hex(comment.task_id)
    comment_dict['parent_comment_id'] = get_uuid_hex(comment.parent_comment_id)
    comment_dict['owner'] = comment.user_id
    return json.dumps(comment_dict), 200

@app.route('/comment/delete', methods=['DELETE'])
@jwt_required()
def delete_comment():
    """[summary]

    Returns:
        [type]: [description]
    """
    # Get the comment
    comment_id = request.args.get('id')
    comment = Comment.query.get(comment_id)

    if comment is None:
        return "That comment id does not exist.", 400

    # Do not proceed if this is not this user's data.
    if uuid.UUID(get_jwt_identity()) != comment.user_id:
        return Response(status=401, mimetype='application/json')

    db.session.delete(comment)
    db.session.commit()

    return Response(status=200, mimetype='application/json')
