"""
All (or most?) config work is placed in this module
"""
import os
from datetime import timedelta

from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager

# These are currently used in login, demo and refresh routes.
ACCESS_EXPIRES = timedelta(minutes=15)
REFRESH_EXPIRES = timedelta(days=1)

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = os.environ['SECRET_KEY']
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = ACCESS_EXPIRES
app.config["JWT_REFRESH_TOKEN_EXPIRES"] = REFRESH_EXPIRES
# These are only needed if we don't proxy
# app.config['JWT_COOKIE_SAMESITE'] = "None"
# app.config['JWT_COOKIE_SECURE'] = True

app.config.from_pyfile(os.environ['SPECTASKULAR_SETTINGS'])
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']

# CORS config
CORS(
    app,
    supports_credentials=True
)

# DB config
db = SQLAlchemy(app)
migrate = Migrate(app, db, compare_type=True)

#flask-jwt-extended config
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
jwt = JWTManager(app)

# We need to import this down here because Python loads
# run.py, which imports app from this file, which will then
# try and import models and routes, which import the db and app
# variable from here, so they need to be intialized first. *exhale*

#pylint: disable=wrong-import-position
from project.api import routes  # noqa E402
