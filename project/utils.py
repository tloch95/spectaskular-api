"""
This module holds various utility functions that can be used around the Spectaskular project.
"""
import uuid

def get_uuid_hex(uuid_to_hex):
    """Get the hex value of a UUID, or None.

    Args:
        uuid_to_hex (UUID): The uuid we want the hex value from.

    Raises:
        ValueError: Raises ValueError if the arg passed in is not of the UUID type.

    Returns:
        str: The hex value of the UUID.
    """
    if uuid_to_hex is None:
        return None
    if not isinstance(uuid_to_hex, uuid.UUID):
        raise ValueError("get_json_serializable_uuid was called with non-UUID type.")
    return uuid_to_hex.hex
        