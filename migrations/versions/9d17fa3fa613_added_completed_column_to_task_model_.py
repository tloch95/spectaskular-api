"""Added completed column to task model. How did I miss this?

Revision ID: 9d17fa3fa613
Revises: 8607d340aa20
Create Date: 2020-09-16 22:32:39.918530

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9d17fa3fa613'
down_revision = '8607d340aa20'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('task', sa.Column('completed', sa.Boolean(), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('task', 'completed')
    # ### end Alembic commands ###
