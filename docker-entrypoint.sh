#!/bin/bash

set -e
flask db upgrade
gunicorn --bind 0.0.0.0:${PORT} project.wsgi:app
